**Unofficial** PHP implementation of Transport Canada's Pleasure Craft Operator Competency Database System API.

---

##### Packagist

> https://packagist.org/packages/transport-canada-php/pcoc

##### GitLab

> https://gitlab.com/transport-canada-php/pcoc  
> ![](https://gitlab.com/transport-canada-php/pcoc/badges/master/build.svg?style=flat-square)  

##### StyleCI 

> https://gitlab.styleci.io/repos/4836521  
> ![](https://gitlab.styleci.io/repos/4836521/shield?branch=master)  