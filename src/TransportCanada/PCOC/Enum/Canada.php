<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Enum;

/**
 * Namespace for province enums.
 */
class Canada implements Enum
{
    /**
     * Alberta.
     *
     * @var string
     */
    const AB = Provinces::AB;

    /**
     * British Columbia.
     *
     * @var string
     */
    const BC = Provinces::BC;

    /**
     * Manitoba.
     *
     * @var string
     */
    const MB = Provinces::MB;

    /**
     * New Brunswick.
     *
     * @var string
     */
    const NB = Provinces::NB;

    /**
     * Newfoundland and Labrador.
     *
     * @var string
     */
    const NL = Provinces::NL;

    /**
     * Northwest Territories.
     *
     * @var string
     */
    const NT = Provinces::NT;

    /**
     * Nova Scotia.
     *
     * @var string
     */
    const NS = Provinces::NS;

    /**
     * Nunavut.
     *
     * @var string
     */
    const NU = Provinces::NU;

    /**
     * Ontario.
     *
     * @var string
     */
    const ON = Provinces::ON;

    /**
     * Prince Edward Island.
     *
     * @var string
     */
    const PE = Provinces::PE;

    /**
     * Quebec.
     *
     * @var string
     */
    const QC = Provinces::QC;

    /**
     * Saskatchewan.
     *
     * @var string
     */
    const SK = Provinces::SK;

    /**
     * Yukon.
     *
     * @var string
     */
    const YT = Provinces::YT;

    /**
     * Returns a list of localized labels for each enum.
     *
     * @param string $lang Language code
     *
     * @return array
     */
    public static function labels(string $lang = 'en')
    {
        // Define English translations
        $en = [
            self::AB => 'Alberta',
            self::BC => 'British Columbia',
            self::MB => 'Manitoba',
            self::NB => 'New Brunswick',
            self::NL => 'Newfoundland and Labrador',
            self::NT => 'Northwest Territories',
            self::NS => 'Nova Scotia',
            self::NU => 'Nunavut',
            self::ON => 'Ontario',
            self::PE => 'Prince Edward Island',
            self::QC => 'Quebec',
            self::SK => 'Saskatchewan',
            self::YT => 'Yukon',
        ];

        // Define French translations
        $fr = [
            self::AB => 'Alberta',
            self::BC => 'Colombie-Britannique',
            self::MB => 'Manitoba',
            self::NB => 'Nouveau-Brunswick',
            self::NL => 'Terre-Neuve-et-Labrador',
            self::NT => 'Territoires du Nord-Ouest',
            self::NS => 'Nouvelle-Écosse',
            self::NU => 'Nunavut',
            self::ON => 'Ontario',
            self::PE => 'Île-du-Prince-Édouard',
            self::QC => 'Québec',
            self::SK => 'Saskatchewan',
            self::YT => 'Yukon',
        ];

        // Using a variable variable, we can access either the French or English
        // translations just by calling the function with either declared variable
        // names (e.g.) Enum\Provinces::labels('fr');
        return $$lang;
    }

    /**
     * Returns the list of enums in this class.
     *
     * @return string[]
     */
    public static function enums()
    {
        return [
            self::AB,
            self::BC,
            self::MB,
            self::NB,
            self::NL,
            self::NT,
            self::NS,
            self::NU,
            self::ON,
            self::PE,
            self::QC,
            self::SK,
            self::YT,
        ];
    }
}
