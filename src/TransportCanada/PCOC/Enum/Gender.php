<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Enum;

/**
 * Namespace for gender enums.
 */
class Gender implements Enum
{
    /**
     * Female enum.
     *
     * @var string
     */
    const FEMALE = 'FEMALE';

    /**
     * Male enum.
     *
     * @var string
     */
    const MALE = 'MALE';

    /**
     * Unknown/other enum.
     *
     * @var string
     */
    const UNKNOWN = 'UNKNOWN';

    /**
     * Returns a list of localized labels for each enum.
     *
     * @param string $lang Language code
     *
     * @return array
     */
    public static function labels(string $lang = 'en')
    {
        $en = [
            self::FEMALE => 'Female',
            self::MALE => 'Male',
            self::UNKNOWN => 'Unknown',
        ];

        $fr = [
            self::FEMALE => 'Female',
            self::MALE => 'Male',
            self::UNKNOWN => 'Inconue',
        ];

        // Variable variable
        return $$lang;
    }

    /**
     * Returns the list of enums in this class.
     *
     * @return string[]
     */
    public static function enums()
    {
        return [
            self::FEMALE,
            self::MALE,
            self::UNKNOWN,
        ];
    }
}
