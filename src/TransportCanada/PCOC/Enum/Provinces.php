<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Enum;

/**
 * Namespace for province code enums.
 */
class Provinces implements Enum
{
    /**
     * Alberta.
     *
     * @var string
     */
    const AB = 'AB';

    /**
     * British Columbia.
     *
     * @var string
     */
    const BC = 'BC';

    /**
     * Manitoba.
     *
     * @var string
     */
    const MB = 'MB';

    /**
     * New Brunswick.
     *
     * @var string
     */
    const NB = 'NB';

    /**
     * Newfoundland and Labrador.
     *
     * @var string
     */
    const NL = 'NL';

    /**
     * Northwest Territories.
     *
     * @var string
     */
    const NT = 'NT';

    /**
     * Nova Scotia.
     *
     * @var string
     */
    const NS = 'NS';

    /**
     * Nunavut.
     *
     * @var string
     */
    const NU = 'NU';

    /**
     * Ontario.
     *
     * @var string
     */
    const ON = 'ON';

    /**
     * Prince Edward Island.
     *
     * @var string
     */
    const PE = 'PE';

    /**
     * Quebec.
     *
     * @var string
     */
    const QC = 'QC';

    /**
     * Saskatchewan.
     *
     * @var string
     */
    const SK = 'SK';

    /**
     * Yukon.
     *
     * @var string
     */
    const YT = 'YT';

    /**
     * Alabama.
     *
     * @var string
     */
    const AL = 'AL';

    /**
     * Alaska.
     *
     * @var string
     */
    const AK = 'AK';

    /**
     * American Samoa.
     *
     * @var string
     */
    const AS = 'AS';

    /**
     * Arizona.
     *
     * @var string
     */
    const AZ = 'AZ';

    /**
     * Arkansas.
     *
     * @var string
     */
    const AR = 'AR';

    /**
     * California.
     *
     * @var string
     */
    const CA = 'CA';

    /**
     * Colorado.
     *
     * @var string
     */
    const CO = 'CO';

    /**
     * Connecticut.
     *
     * @var string
     */
    const CT = 'CT';

    /**
     * Delaware.
     *
     * @var string
     */
    const DE = 'DE';

    /**
     * District of Columbia.
     *
     * @var string
     */
    const DC = 'DC';

    /**
     * Federated States of Micronesia.
     *
     * @var string
     */
    const FM = 'FM';

    /**
     * Florida.
     *
     * @var string
     */
    const FL = 'FL';

    /**
     * Georgia.
     *
     * @var string
     */
    const GA = 'GA';

    /**
     * Guam.
     *
     * @var string
     */
    const GU = 'GU';

    /**
     * Hawaii.
     *
     * @var string
     */
    const HI = 'HI';

    /**
     * Idaho.
     *
     * @var string
     */
    const ID = 'ID';

    /**
     * Illinois.
     *
     * @var string
     */
    const IL = 'IL';

    /**
     * Indiana.
     *
     * @var string
     */
    const IN = 'IN';

    /**
     * Iowa.
     *
     * @var string
     */
    const IA = 'IA';

    /**
     * Kansas.
     *
     * @var string
     */
    const KS = 'KS';

    /**
     * Kentucky.
     *
     * @var string
     */
    const KY = 'KY';

    /**
     * Louisiana.
     *
     * @var string
     */
    const LA = 'LA';

    /**
     * Maine.
     *
     * @var string
     */
    const ME = 'ME';

    /**
     * Marshall Islands.
     *
     * @var string
     */
    const MH = 'MH';

    /**
     * Maryland.
     *
     * @var string
     */
    const MD = 'MD';

    /**
     * Massachusetts.
     *
     * @var string
     */
    const MA = 'MA';

    /**
     * Michigan.
     *
     * @var string
     */
    const MI = 'MI';

    /**
     * Minnesota.
     *
     * @var string
     */
    const MN = 'MN';

    /**
     * Mississippi.
     *
     * @var string
     */
    const MS = 'MS';

    /**
     * Missouri.
     *
     * @var string
     */
    const MO = 'MO';

    /**
     * Montana.
     *
     * @var string
     */
    const MT = 'MT';

    /**
     * Nebraska.
     *
     * @var string
     */
    const NE = 'NE';

    /**
     * Nevada.
     *
     * @var string
     */
    const NV = 'NV';

    /**
     * New Hampshire.
     *
     * @var string
     */
    const NH = 'NH';

    /**
     * New Jersey.
     *
     * @var string
     */
    const NJ = 'NJ';

    /**
     * New Mexico.
     *
     * @var string
     */
    const NM = 'NM';

    /**
     * New York.
     *
     * @var string
     */
    const NY = 'NY';

    /**
     * North Carolina.
     *
     * @var string
     */
    const NC = 'NC';

    /**
     * North Dakota.
     *
     * @var string
     */
    const ND = 'ND';

    /**
     * Northern Mariana Island.
     *
     * @var string
     */
    const MP = 'MP';

    /**
     * Ohio.
     *
     * @var string
     */
    const OH = 'OH';

    /**
     * Oklahoma.
     *
     * @var string
     */
    const OK = 'OK';

    /**
     * Oregon.
     *
     * @var string
     */
    const OR = 'OR';

    /**
     * Palau.
     *
     * @var string
     */
    const PW = 'PW';

    /**
     * Pennsylvania.
     *
     * @var string
     */
    const PA = 'PA';

    /**
     * Puerto Rico.
     *
     * @var string
     */
    const PR = 'PR';

    /**
     * Rhode Island.
     *
     * @var string
     */
    const RI = 'RI';

    /**
     * South Carolina.
     *
     * @var string
     */
    const SC = 'SC';

    /**
     * South Dakota.
     *
     * @var string
     */
    const SD = 'SD';

    /**
     * Tennessee.
     *
     * @var string
     */
    const TN = 'TN';

    /**
     * Texas.
     *
     * @var string
     */
    const TX = 'TX';

    /**
     * Utah.
     *
     * @var string
     */
    const UT = 'UT';

    /**
     * Vermont.
     *
     * @var string
     */
    const VT = 'VT';

    /**
     * Virgin islands.
     *
     * @var string
     */
    const VI = 'VI';

    /**
     * Virginia.
     *
     * @var string
     */
    const VA = 'VA';

    /**
     * Washington.
     *
     * @var string
     */
    const WA = 'WA';

    /**
     * West Virginia.
     *
     * @var string
     */
    const WV = 'WV';

    /**
     * Wisconsin.
     *
     * @var string
     */
    const WI = 'WI';

    /**
     * Wyoming.
     *
     * @var string
     */
    const WY = 'WY';

    /**
     * Outside Canada / US.
     *
     * @var string
     */
    const ZZ = 'ZZ';

    /**
     * Returns a list of localized labels for each enum.
     *
     * @param string $lang Language code
     *
     * @return array
     */
    public static function labels(string $lang = 'en')
    {
        // Define English translations
        $en = [
            self::AB => 'Alberta',
            self::BC => 'British Columbia',
            self::MB => 'Manitoba',
            self::NB => 'New Brunswick',
            self::NL => 'Newfoundland and Labrador',
            self::NT => 'Northwest Territories',
            self::NS => 'Nova Scotia',
            self::NU => 'Nunavut',
            self::ON => 'Ontario',
            self::PE => 'Prince Edward Island',
            self::QC => 'Quebec',
            self::SK => 'Saskatchewan',
            self::YT => 'Yukon',

            self::AL => 'Alabama',
            self::AK => 'Alaska',
            self::AS => 'American Samoa',
            self::AZ => 'Arizona',
            self::AR => 'Arkansas',
            self::CA => 'California',
            self::CO => 'Colorado',
            self::CT => 'Connecticut',
            self::DE => 'Delaware',
            self::DC => 'District of Columbia',
            self::FM => 'Federated States of Micronesia',
            self::FL => 'Florida',
            self::GA => 'Georgia',
            self::GU => 'Guam',
            self::HI => 'Hawaii',
            self::ID => 'Idaho',
            self::IL => 'Illinois',
            self::IN => 'Indiana',
            self::IA => 'Iowa',
            self::KS => 'Kansas',
            self::KY => 'Kentucky',
            self::LA => 'Louisiana',
            self::ME => 'Maine',
            self::MH => 'Marshall Islands',
            self::MD => 'Maryland',
            self::MA => 'Massachusetts',
            self::MI => 'Michigan',
            self::MN => 'Minnesota',
            self::MS => 'Mississippi',
            self::MO => 'Missouri',
            self::MT => 'Montana',
            self::NE => 'Nebraska',
            self::NV => 'Nevada',
            self::NH => 'New Hampshire',
            self::NJ => 'New Jersey',
            self::NM => 'New Mexico',
            self::NY => 'New York',
            self::NC => 'North Carolina',
            self::ND => 'North Dakota',
            self::MP => 'Northern Mariana Island',
            self::OH => 'Ohio',
            self::OK => 'Oklahoma',
            self::OR => 'Oregon',
            self::PW => 'Palau',
            self::PA => 'Pennsylvania',
            self::PR => 'Puerto Rico',
            self::RI => 'Rhode Island',
            self::SC => 'South Carolina',
            self::SD => 'South Dakota',
            self::TN => 'Tennessee',
            self::TX => 'Texas',
            self::UT => 'Utah',
            self::VT => 'Vermont',
            self::VI => 'Virgin islands',
            self::VA => 'Virginia',
            self::WA => 'Washington',
            self::WV => 'West Virginia',
            self::WI => 'Wisconsin',
            self::WY => 'Wyoming',

            self::ZZ => 'Outside Canada / US',
        ];

        // Define French translations
        $fr = [
            self::AB => 'Alberta',
            self::BC => 'Colombie-Britannique',
            self::MB => 'Manitoba',
            self::NB => 'Nouveau-Brunswick',
            self::NL => 'Terre-Neuve-et-Labrador',
            self::NT => 'Territoires du Nord-Ouest',
            self::NS => 'Nouvelle-Écosse',
            self::NU => 'Nunavut',
            self::ON => 'Ontario',
            self::PE => 'Île-du-Prince-Édouard',
            self::QC => 'Québec',
            self::SK => 'Saskatchewan',
            self::YT => 'Yukon',

            self::AL => 'Alabama',
            self::AK => 'Alaska',
            self::AS => 'American Samoa',
            self::AZ => 'Arizona',
            self::AR => 'Arkansas',
            self::CA => 'California',
            self::CO => 'Colorado',
            self::CT => 'Connecticut',
            self::DE => 'Delaware',
            self::DC => 'District of Columbia',
            self::FM => 'Federated States of Micronesia',
            self::FL => 'Florida',
            self::GA => 'Georgia',
            self::GU => 'Guam',
            self::HI => 'Hawaii',
            self::ID => 'Idaho',
            self::IL => 'Illinois',
            self::IN => 'Indiana',
            self::IA => 'Iowa',
            self::KS => 'Kansas',
            self::KY => 'Kentucky',
            self::LA => 'Louisiana',
            self::ME => 'Maine',
            self::MH => 'Marshall Islands',
            self::MD => 'Maryland',
            self::MA => 'Massachusetts',
            self::MI => 'Michigan',
            self::MN => 'Minnesota',
            self::MS => 'Mississippi',
            self::MO => 'Missouri',
            self::MT => 'Montana',
            self::NE => 'Nebraska',
            self::NV => 'Nevada',
            self::NH => 'New Hampshire',
            self::NJ => 'New Jersey',
            self::NM => 'New Mexico',
            self::NY => 'New York',
            self::NC => 'North Carolina',
            self::ND => 'North Dakota',
            self::MP => 'Northern Mariana Island',
            self::OH => 'Ohio',
            self::OK => 'Oklahoma',
            self::OR => 'Oregon',
            self::PW => 'Palau',
            self::PA => 'Pennsylvania',
            self::PR => 'Puerto Rico',
            self::RI => 'Rhode Island',
            self::SC => 'South Carolina',
            self::SD => 'South Dakota',
            self::TN => 'Tennessee',
            self::TX => 'Texas',
            self::UT => 'Utah',
            self::VT => 'Vermont',
            self::VI => 'Virgin islands',
            self::VA => 'Virginia',
            self::WA => 'Washington',
            self::WV => 'West Virginia',
            self::WI => 'Wisconsin',
            self::WY => 'Wyoming',

            self::ZZ => 'Hors du Canada / US',
        ];

        // Using a variable variable, we can access either the French or English
        // translations just by calling the function with either declared variable
        // names (e.g.) Enum\Provinces::labels('fr');
        return $$lang;
    }

    /**
     * Returns the list of enums in this class.
     *
     * @return string[]
     */
    public static function enums()
    {
        return [
            self::AB,
            self::BC,
            self::MB,
            self::NB,
            self::NL,
            self::NT,
            self::NS,
            self::NU,
            self::ON,
            self::PE,
            self::QC,
            self::SK,
            self::YT,

            self::AL,
            self::AK,
            self::AS,
            self::AZ,
            self::AR,
            self::CA,
            self::CO,
            self::CT,
            self::DE,
            self::DC,
            self::FM,
            self::FL,
            self::GA,
            self::GU,
            self::HI,
            self::ID,
            self::IL,
            self::IN,
            self::IA,
            self::KS,
            self::KY,
            self::LA,
            self::ME,
            self::MH,
            self::MD,
            self::MA,
            self::MI,
            self::MN,
            self::MS,
            self::MO,
            self::MT,
            self::NE,
            self::NV,
            self::NH,
            self::NJ,
            self::NM,
            self::NY,
            self::NC,
            self::ND,
            self::MP,
            self::OH,
            self::OK,
            self::OR,
            self::PW,
            self::PA,
            self::PR,
            self::RI,
            self::SC,
            self::SD,
            self::TN,
            self::TX,
            self::UT,
            self::VT,
            self::VI,
            self::VA,
            self::WA,
            self::WV,
            self::WI,
            self::WY,

            self::ZZ,
        ];
    }
}
