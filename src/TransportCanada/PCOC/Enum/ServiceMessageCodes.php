<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Enum;

/**
 * Namespace for service message codes.
 *
 * Note this isn't part of the normal enums. Although codes are constants, their messages aren't. A TC0004 has five different reasons why Transport Canada ultimately rejected the date of birth we provided.
 *
 * These are the possible message
 * codes that Transport Canada can return. You will need to iterate over the
 * returned service messages to check if their code contains any of the declared
 * constants and the reason message they return.
 *
 * Note that service messages contain additional information other than a single definition for the code. As an example, finding a TC0004(invalid date of birth) in the service messages contains five different variations of the same message. Although the extended messages have purpose, they essentially prevent us from making Transport Canada service messages into an enum class.
 *
 * Note there are three different "labels"
 *   1. based on the developer's intepretation of the message for debugging
 *   2. based on the stored official responses
 */
class ServiceMessageCodes implements Enum
{
    /**
     * Error for invalid SAIK.
     *
     * @var string
     */
    const TC0000 = 'TC0000';

    /**
     * Error for expired SAIK.
     *
     * @var string
     */
    const TC0001 = 'TC0001';

    /**
     * Error for invalid operator address city.
     *
     * @var string
     */
    const TC0002 = 'TC0002';

    /**
     * Error for invalid operator address country.
     */
    const TC0003 = 'TC0003';

    /**
     * Error for invalid operator birth date.
     */
    const TC0004 = 'TC0004';

    /**
     * Error for invalid operator email.
     */
    const TC0005 = 'TC0005';

    /**
     * Error for invalid operator first name.
     */
    const TC0006 = 'TC0006';

    /**
     * Error for invalid operator gender code.
     */
    const TC0007 = 'TC0007';

    /**
     * Error for invalid operator last name.
     */
    const TC0008 = 'TC0008';

    /**
     * INACTIVE exam started but not complete.
     */
    const TC0009 = 'TC0009';

    /**
     * Error for invalid operator address postal.
     */
    const TC0010 = 'TC0010';

    /**
     * Error for invalid operator provide code.
     */
    const TC0011 = 'TC0011';

    /**
     * Warning for fetching results with an invalid token.
     */
    const TC0012 = 'TC0012';

    /**
     * INACTIVE issuing new token due to expiration.
     */
    const TC0013 = 'TC0013';

    /**
     * Error for failed API login.
     */
    const TC0014 = 'TC0014';

    /**
     * Warning for operator already having a PCOC.
     */
    const TC0015 = 'TC0015';

    /**
     * INACTIVE invalid code for token.
     */
    const TC0016 = 'TC0016';

    /**
     * Warning for new operators.
     */
    const TC0017 = 'TC0017';

    /**
     * Warning for existing operators.
     */
    const TC0018 = 'TC0018';

    /**
     * Warning for operator failing exam in last 24 hours.
     */
    const TC0019 = 'TC0019';

    /**
     * Error for invalid operator address line 1.
     */
    const TC0020 = 'TC0020';

    /**
     * INACTIVE operator failed twice.
     */
    const TC0021 = 'TC0021';

    /**
     * Warning for provider not authorized to use requested exam language.
     */
    const TC0022 = 'TC0022';

    /**
     * Warning for exam not started.
     */
    const TC0023 = 'TC0023';

    /**
     * Warning for SAIK soon expiring.
     */
    const TC0024 = 'TC0024';

    /**
     * Warning for requesting exam retries.
     */
    const TC0025 = 'TC0025';

    /**
     * Error for database being offline.
     */
    const TC0028 = 'TC0028';

    /**
     * UNDOCUMENTED Warning for users having passed.
     */
    const TC0029 = 'TC0029';

    /**
     * Returns a list of shortened localized messages for each enum.
     *
     * @param string $lang Language code
     *
     * @return array
     */
    public static function labels(string $lang = 'en')
    {
        $en = [
            self::TC0000 => 'Invalid SAIK',
            self::TC0001 => 'Expired SAIK',
            self::TC0002 => 'Invalid operator address city',
            self::TC0003 => 'Invalid operator address country',
            self::TC0004 => 'Invalid operator birth date',
            self::TC0005 => 'Invalid operator email',
            self::TC0006 => 'Invalid operator first name',
            self::TC0007 => 'Invalid operator gender code',
            self::TC0008 => 'Invalid operator last name',
            self::TC0009 => '[INACTIVE] Exam started but not complete',
            self::TC0010 => 'Invalid operator address postal',
            self::TC0011 => 'Invalid operator provide code',
            self::TC0012 => 'Fetching results with an invalid token',
            self::TC0013 => '[INACTIVE] Issuing new token due to expiration',
            self::TC0014 => 'Failed API login',
            self::TC0015 => 'Operator already has a PCOC',
            self::TC0016 => '[INACTIVE] Invalid code for token',
            self::TC0017 => 'New operator',
            self::TC0018 => 'Existing operator',
            self::TC0019 => 'Operator failed exam in last 24 hours',
            self::TC0020 => 'Invalid operator address line 1',
            self::TC0021 => '[INACTIVE] Operator failed twice',
            self::TC0022 => 'Provider not authorized to use requested exam language',
            self::TC0023 => 'Exam not started',
            self::TC0024 => 'SAIK expires soon',
            self::TC0025 => 'Requesting exam retry',
            self::TC0028 => 'Database offline',
            self::TC0029 => 'The Candidate has passed a test',
        ];

        $fr = [
            self::TC0000 => 'CIAS invalide',
            self::TC0001 => 'CIAS expiré',
            self::TC0002 => 'Ville de l\'opérateur invalide',
            self::TC0003 => 'Pays de l\'opérateur invalide',
            self::TC0004 => 'Date de naissance de l\'opérateur invalide',
            self::TC0005 => 'Email de l\'opérateur invalide',
            self::TC0006 => 'Prénom opérateur invalide',
            self::TC0007 => 'Code de genre d\'opérateur invalide',
            self::TC0008 => 'Nom de famille de l\'opérateur invalide',
            self::TC0009 => '[INACTIF] L\'examen a commencé mais n\'est pas terminé',
            self::TC0010 => 'Adresse postale de l\'opérateur invalide',
            self::TC0011 => 'Code de province de l\'opérateur invalide',
            self::TC0012 => 'Jeton invalide pour la récupération des résultats',
            self::TC0013 => '[INACTIF] Émission d\'un nouveau jeton depuis son expiration',
            self::TC0014 => 'Echec de la connexion à l\'API',
            self::TC0015 => 'L\'opérateur a déjà un CCEP',
            self::TC0016 => '[INACTIF] Code invalide pour le jeton',
            self::TC0017 => 'Nouvel opérateur',
            self::TC0018 => 'Opérateur existant',
            self::TC0019 => 'Opérateur a échoué l\'examen au cours des dernières 24 heures',
            self::TC0020 => 'Première ligne d\'adresse de l\'opérateur invalide',
            self::TC0021 => '[INACTIF] L\'opérateur a échoué deux fois',
            self::TC0022 => 'Fournisseur n\'est pas autorisé à utiliser la langue d\'examen demandée',
            self::TC0023 => 'Examen non commencé',
            self::TC0024 => 'CIAS expire bientôt',
            self::TC0025 => 'Demande de nouvelle tentative d’examen',
            self::TC0028 => 'Base de données hors ligne',
            self::TC0029 => 'Le candidat à passé l\'examen',
        ];

        return $$lang;
    }

    /**
     * Returns the list of enums in this class.
     *
     * @return string[]
     */
    public static function enums()
    {
        return [
            self::TC0000,
            self::TC0001,
            self::TC0002,
            self::TC0003,
            self::TC0004,
            self::TC0005,
            self::TC0006,
            self::TC0007,
            self::TC0008,
            self::TC0009,
            self::TC0010,
            self::TC0011,
            self::TC0012,
            self::TC0013,
            self::TC0014,
            self::TC0015,
            self::TC0016,
            self::TC0017,
            self::TC0018,
            self::TC0019,
            self::TC0020,
            self::TC0021,
            self::TC0022,
            self::TC0023,
            self::TC0024,
            self::TC0025,
            self::TC0028,
            self::TC0029,
        ];
    }

    /**
     * Returns a list of localized messages from Transport Canada for each enum with possible variations for each.
     *
     * Transport Canada's response have some dynamic messages where the enum is further clarified. Instead of accounting for each variant we put the base message as the first index.
     *
     * @param string $lang Language code
     *
     * @return array
     */
    public static function verbose(string $lang = 'en')
    {
        $en = [
            self::TC0000 => [
                'The SAIK value provided as part of the authorization request is not valid',
                'SAIK format is not valid',
                'SAIK was not provided',
            ],
            self::TC0001 => [
                'The SAIK provided has expired',
            ],
            self::TC0002 => [
                'The city provided is not valid',
                'City name is too many characters',
                'City name was not provided',
            ],
            self::TC0003 => [
                'The country code provided is not valid',
                'Country code is too many characters',
                'Country code was not provided',
            ],
            self::TC0004 => [
                'The date of birth provided is not valid',
                'Date of birth is in the future',
                'Date of birth is too far in the past',
                'Date of birth is too recent',
                'Date of birth was not provided',
                'Date of Birth is not in a valid format',
            ],
            self::TC0005 => [
                'The email provided is not valid',
                'Email is too many characters',
                'Email was not provided',
                'Email is not in a valid format',
            ],
            self::TC0006 => [
                'The first name provided is not valid',
                'First name is too many characters',
                'First name was not provided',
                'First name is not in a valid format',
            ],
            self::TC0007 => [
                'The gender code provided is not valid',
                'Gender code is too many characters',
                'Gender code was not provided',
                'Gender code is not in a valid format',
            ],
            self::TC0008 => [
                'The last name provided is not valid',
                'Last name is too many characters',
                'Last name was not provided',
                'Last name is not in a valid format',
            ],
            self::TC0009 => [
                '[INACTIVE] The candidate has a valid but incomplete test',
            ],
            self::TC0010 => [
                'The postal/zip code provided is not valid',
                'Postal/zip code is too many characters',
                'Postal/zip code was not provided',
                'Postal/zip code is not in a valid format',
            ],
            self::TC0011 => [
                'The province/state code provided is not valid',
                'Province/state code is too many characters',
                'Province/state code was not provided',
                'Province/state code is not in a valid format',
            ],
            self::TC0012 => [
                'Unable to locate results for the token value provided',
            ],
            self::TC0013 => [
                '[INACTIVE] The current token has expired. A new token has been issued. See URL',
            ],
            self::TC0014 => [
                'Login failed',
                'Account has expired',
                'Username or password was not provided',
                'Username or password is not valid',
            ],
            self::TC0015 => [
                'A candidate with the same first name, last name, date of birth and complete address was found and has been issued a pleasure craft operator card',
            ],
            self::TC0016 => [
                '[INACTIVE] There is an invalid or missing code for the current token',
            ],
            self::TC0017 => [
                'No matching candidates were found given any of the details provided by the authorization request',
            ],
            self::TC0018 => [
                '"n" candidate(s) with the same first name, last name, date of birth and complete address was (were) found',
            ],
            self::TC0019 => [
                'The candidate has failed the test within the last 24 hours',
            ],
            self::TC0020 => [
                'The address value provided is not valid',
                'Address is too many characters',
                'Address was not provided',
                'Address is not in a valid format',
            ],
            self::TC0021 => [
                '[INACTIVE] The candidate failed course twice and should retake the course before using the provided URL',
            ],
            self::TC0022 => [
                'The course provider is not authorized to provide an online test for the language value specified',
            ],
            self::TC0023 => [
                'Candidate already has a valid token but has not started their online test. Please see the TestURL property for the original online test URL',
            ],
            self::TC0024 => [
                'Please be advised that the SAIK you are currently using is set to expire in ‘n’ days',
            ],
            self::TC0025 => [
                'A new test URL has been sent to the candidate for a second test retry. Please see the TestURL property for the new online test URL',
            ],
            self::TC0028 => [
                'The PCOC Database System is currently unavailable, please try again later',
            ],
            self::TC0029 => [
                'Please call GetFinalTestResults with the following token',
            ],
        ];

        $fr = [
            self::TC0000 => [
                'Le format de CIAS n\'est pas valide',
                'La CIAS n\'a pas été fournie',
            ],
            self::TC0001 => [
                'La CIAS fournie est expirée',
            ],
            self::TC0002 => [
                'La ville fournie n\'est pas valide',
                'Le nom de la ville est trop long',
                'Le nom de la ville n\'a pas été fourni',
            ],
            self::TC0003 => [
                'Le code de pays fourni n\'est pas valide',
                'Le code de pays est trop long',
                'Le code de pays n\'a pas été fourni',
            ],
            self::TC0004 => [
                'La date de naissance fournie n\'est pas valide',
                'La date de naissance est dans le futur',
                'La date de naissance se situe trop loin dans le passé',
                'La date de naissance est trop récente',
                'La date de naissance n\'a pas été fournie',
                'La date de naissance n\'est pas formattée correctement',
            ],
            self::TC0005 => [
                'Le courriel fourni n\'est pas valide',
                'Le courriel fourni est trop long',
                'Le courriel n\'a pas été fourni',
                'Le format du courriel n\'est pas valide',
            ],
            self::TC0006 => [
                'Le prénom fourni n\'est pas valide',
                'Le prénom fourni est trop long',
                'Le prénom n\'a pas été fourni',
                'Le format du prénom est invalide',
            ],
            self::TC0007 => [
                'Le genre fourni n\'est pas valide',
                'Le code du genre est trop long',
                'Le code du genre n\'a pas été fourni',
                'Le code du genre n\'est pas formatté correctement',
            ],
            self::TC0008 => [
                'Le nom de famille fourni n\'est pas valide',
                'Le nom de famille fourni est trop long',
                'Le nom de famille n\'a pas été fourni',
                'Le nom de famille n\'est pas formatté correctement',
            ],
            self::TC0009 => [
                '[INACTIF] Le candidat a un examen valide mais incomplet',
            ],
            self::TC0010 => [
                'Le code postal ou zip n\'est pas valide',
                'Le code postal ou zip est trop long',
                'Le code postal ou zip n\'a pas été fourni',
                'Le code postal ou zip n\'est pas formatté correctement',
            ],
            self::TC0011 => [
                'Le code de province ou d\'état fourni n\'est pas valide',
                'Le code de province ou état est trop long',
                'Le code de province ou état n\'a pas été fourni',
                'Le code de province ou état n\'est pas formatté correctement',
            ],
            self::TC0012 => [
                'Impossible de repérer les résultats pour le jeton fourni',
            ],
            self::TC0013 => [
                '[INACTIF] Le jeton en cours a expiré. Un nouveau jeton a été fourni. Voir URL',
            ],
            self::TC0014 => [
                'Identification échouée',
                'Compte expiré',
                'Le nom d\'utilisateur ou mot de passe n\'a pas été fourni',
                'Nom d\'utilisateur ou mot de passe invalide',
            ],
            self::TC0015 => [
                'Un candidat avec le même prénom, nom de famille, date de naissance et adresse complète a été retrouvé et semble avoir un numéro de carte de conducteur d\'embarcation de plaisance',
            ],
            self::TC0016 => [
                '[INACTIF] Il y a un code invalide ou manquant pour le jeton en cours',
            ],
            self::TC0017 => [
                'Aucun candidat correspondant ne fut trouvé compte tenu des détails fournis pas la demande d\'autorisation',
            ],
            self::TC0018 => [
                '"n" candidat(s) avec le même prénom, nom de famille, date de naissance et adresse complète a(ont) été trouvé(s)',
            ],
            self::TC0019 => [
                'Le candidat a échoué l\'examen durant les derniers 24 heures',
            ],
            self::TC0020 => [
                'La valeur de l\'adresse fournie n\'est pas valide',
                'L\'adresse est trop longue',
                'L\'adresse n\'a pas été fournie',
                'L\'adresse n\'est pas formattée correctement',
            ],
            self::TC0021 => [
                '[INACTIF] Le candidat a échoué l’examen deux fois et devra reprendre le cours avant d’utiliser l’adresse (URL) fournie',
            ],
            self::TC0022 => [
                'Le prestataire de cours n\'est pas autorisé de fournir un examen dans la langue spécifiée',
            ],
            self::TC0023 => [
                'Le candidat a déjà un jeton valide mais n\'a pas démarré son examen. SVP voir la propriété TestURL pour voir l\'adresse de son examen en ligne original',
            ],
            self::TC0024 => [
                'SVP soyez avisés que la CIAS que vous utilisez présentement sera expirée dans ‘n’ jours',
            ],
            self::TC0025 => [
                'Une nouvelle adresse (URL) a été envoyée au candidat pour un second essai. SVP voir la propriété TestURL pour la nouvelle adresse (URL) du nouvel examen en ligne',
            ],
            self::TC0028 => [
                'Le système de bases de données du CCEP n\'est présentement pas disponible, SVP ré-essayez plus tard',
            ],
            self::TC0029 => [
                'Veuillez appeler GetFinalTestResults avec le jeton suivant',
            ],
        ];

        return $$lang;
    }
}
