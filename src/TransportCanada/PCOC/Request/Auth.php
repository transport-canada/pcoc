<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Request;

use TransportCanada\PCOC\Schema;
use TransportCanada\PCOC\Util;

/**
 * Wrapper for the base request which contains the authentication data.
 */
class Auth extends Schema\Struct\BaseRequestObj
{
    use Util\Lenient;

    /**
     * The SAIK provided to the course provider via PCOC Database System and
     * used to call the Web Service (see 3.3).
     *
     * Usually defined in Schema\Exam::$SAIK but it makes a bit more sense
     * having it associated to authentication(inherited from BaseRequestObj).
     *
     * @var string
     */
    public $SAIK = '';
}
