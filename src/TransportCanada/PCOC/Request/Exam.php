<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Request;

use TransportCanada\PCOC\Schema;
use TransportCanada\PCOC\Util;
use TransportCanada\PCOC\Validate;

/**
 * Wrapper for the Authorize API call.
 */
class Exam extends Schema\Struct\Authorize implements Request
{
    use Util\Lenient;

    /**
     * Shorter property name for testing language.
     *
     * @var string
     */
    const LANGUAGE = 'TestingLanguageCd';

    /**
     * Shorter property name for client operator ID.
     *
     * @var string
     */
    const ORG_ID = 'OrganizationOperatorId';

    /**
     * Shorter property name for operator information.
     *
     * @var string
     */
    const OPERATOR = 'OperatorInformation';

    /**
     * Shorter property name for the return URL.
     *
     * @var string
     */
    const RETURN_URL = 'ReturnUrl';

    /**
     * Converts this exam wrapper into the WSDL defined Authorize struct.
     *
     * @var string
     */
    protected static $decorate = Schema\Struct\Authorize::class;

    /**
     * Validator to use for validating user input.
     *
     * @var string
     */
    protected static $validate = Validate\Exam::class;

    /**
     * Creates a new API request to generate an exam.
     *
     * @param array $attributes Authorization parameter data
     */
    public function __construct($attributes)
    {
        $this->request = new Schema\Authorize($attributes);
    }

    /**
     * Populates our request with API credentials / authentication data.
     *
     * @param \TransportCanada\PCOC\Request\Auth $auth Authentication data
     *
     * @return \TransportCanada\PCOC\Request\Request
     */
    public function auth(Auth $auth)
    {
        $this->request->fill($auth);

        return $this;
    }

    /**
     * Returns the WSDL function this request is associated to.
     *
     * @return string
     */
    public function method()
    {
        return 'Authorize';
    }

    /**
     * Validates the current request object.
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return bool
     */
    public function validate()
    {
        (new Validate\Rule\ObjectRule($this->request))
            ->validate(new static::$validate());

        return true;
    }
}
