<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Request;

use TransportCanada\PCOC\Schema;
use TransportCanada\PCOC\Util;
use TransportCanada\PCOC\Validate;

/**
 * Wrapper for the GetFinalTestResult API call.
 */
class Grade extends Schema\Struct\GetFinalTestResult implements Request
{
    use Util\Lenient;

    /**
     * Converts this exam wrapper into the WSDL defined GetFinalTestResult
     * struct.
     *
     * @var string
     */
    protected static $decorate = Schema\Struct\GetFinalTestResult::class;

    /**
     * Validator to run when preparing the request.
     *
     * @var string
     */
    protected static $validate = Validate\Grade::class;

    /**
     * Creates a new API request to get a operator's final test results.
     *
     * @param array $data Request data
     */
    public function __construct(array $data = [])
    {
        $this->request = new Schema\Results($data);
    }

    /**
     * Populates our request with API credentials / authentication data.
     *
     * @param \TransportCanada\PCOC\Request\Auth $auth Authentication data
     *
     * @return \TransportCanada\PCOC\Request\Request
     */
    public function auth(Auth $auth)
    {
        $this->request->fill($auth);

        return $this;
    }

    /**
     * Returns the WSDL function this request is associated to.
     *
     * @return string
     */
    public function method()
    {
        return 'GetFinalTestResult';
    }

    /**
     * Validates the current request object.
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return bool
     */
    public function validate()
    {
        (new Validate\Rule\ObjectRule($this->request))
            ->validate(new static::$validate());

        return true;
    }
}
