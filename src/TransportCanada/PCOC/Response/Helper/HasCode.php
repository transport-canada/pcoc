<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Response\Helper;

use BadMethodCallException;
use TransportCanada\PCOC\Enum;

/**
 * Helper functions for handling response codes.
 */
trait HasCode
{
    /**
     * Returns an array of messages keyed by their codes.
     *
     * Example:
     *
     * <samp>
     * array(
     *      'TC0000' => 'Service message for TC0000',
     *      'TC0023' => 'Service message for TC0023',
     * );
     * </samp>
     *
     * @return array
     */
    public function codes()
    {
        /* @var \TransportCanada\PCOC\Response\Response $this */
        $codes = [];

        /* @var \TransportCanada\PCOC\Schema\Message $message */
        foreach ($this->messages() as $message) {
            $codes[$message->Code] = $message->Message;
        }

        return $codes;
    }

    /**
     * Checks if our service messages contains a specific code.
     *
     * @param string $code Service message code to check
     *
     * @return bool
     */
    public function hasCode(string $code)
    {
        // Enforce Transport Canada code template
        if (!preg_match('/^TC\d{4}$/', $code)) {
            throw new BadMethodCallException('$code must be a Transport Canada code(e.g. "TC0018")');
        }

        return \array_key_exists($code, $this->codes());
    }

    /**
     * Checks if the system is in maintenance.
     *
     * @return bool
     */
    public function maintenance()
    {
        return $this->hasCode(Enum\ServiceMessageCodes::TC0028);
    }
}
