<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Response\Helper;

/**
 * Helper functions for handling response messages.
 */
trait HasMessage
{
    /**
     * Returns all of the messages associated with the response.
     *
     * @return \TransportCanada\PCOC\Schema\Message[]
     */
    public function messages()
    {
        $messages = $this->results()->ServiceMessages;
        $actual = $messages->ServiceMessageObj ?? [];

        return \is_array($actual) ? $actual : [$actual];
    }
}
