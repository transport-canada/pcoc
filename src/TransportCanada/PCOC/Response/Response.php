<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Response;

use TransportCanada\PCOC\Enum\ServiceMessageCodes as Code;

/**
 * Response interface.
 */
interface Response
{
    /**
     * Error for invalid SAIK.
     *
     * @var string
     */
    const ERROR_SAIK_INVALID = Code::TC0000;

    /**
     * Error for expired SAIK.
     *
     * @var string
     */
    const ERROR_SAIK_EXPIRED = Code::TC0001;

    /**
     * Error for invalid operator address city.
     *
     * @var string
     */
    const ERROR_OPERATOR_INVALID_CITY = Code::TC0002;

    /**
     * Error for invalid operator address country.
     *
     * @var string
     */
    const ERROR_OPERATOR_INVALID_COUNTRY = Code::TC0003;

    /**
     * Error for invalid operator birth date.
     *
     * @var string
     */
    const ERROR_OPERATOR_INVALID_BIRTH_DATE = Code::TC0004;

    /**
     * Error for invalid operator email.
     *
     * @var string
     */
    const ERROR_OPERATOR_INVALID_EMAIL = Code::TC0005;

    /**
     * Error for invalid operator first name.
     *
     * @var string
     */
    const ERROR_OPERATOR_INVALID_FIRST_NAME = Code::TC0006;

    /**
     * Error for invalid operator gender code.
     *
     * @var string
     */
    const ERROR_OPERATOR_INVALID_GENDER = Code::TC0007;

    /**
     * Error for invalid operator last name.
     *
     * @var string
     */
    const ERROR_OPERATOR_INVALID_LAST_NAME = Code::TC0008;

    /**
     * INACTIVE exam started but not complete.
     *
     * @var string
     */
    const INACTIVE_EXAM_NOT_COMPLETE = Code::TC0009;

    /**
     * Error for invalid operator address postal.
     *
     * @var string
     */
    const ERROR_OPERATOR_INVALID_POSTAL = Code::TC0010;

    /**
     * Error for invalid operator provide code.
     *
     * @var string
     */
    const ERROR_OPERATOR_INVALID_PROVINCE = Code::TC0011;

    /**
     * Warning for fetching results with an invalid token.
     *
     * @var string
     */
    const WARNING_RESULT_TOKEN_INVALID = Code::TC0012;

    /**
     * INACTIVE issuing new token due to expiration.
     *
     * @var string
     */
    const INACTIVE_RESULT_TOKEN_EXPIRED = Code::TC0013;

    /**
     * Error for failed API login.
     *
     * @var string
     */
    const ERROR_EXAM_AUTH_FAILED = Code::TC0014;

    /**
     * Warning for operator already having a PCOC.
     *
     * @var string
     */
    const WARNING_OPERATOR_HAS_PCOC = Code::TC0015;

    /**
     * INACTIVE invalid code for token.
     *
     * @var string
     */
    const INACTIVE_RESULT_TOKEN_INVALID_CODE = Code::TC0016;

    /**
     * Warning for new operators.
     *
     * @var string
     */
    const WARNING_OPERATOR_NEW = Code::TC0017;

    /**
     * Warning for existing operators.
     *
     * @var string
     */
    const WARNING_OPERATOR_EXISTS = Code::TC0018;

    /**
     * Warning for operator failing exam in last 24 hours.
     *
     * @var string
     */
    const WARNING_EXAM_FAILED = Code::TC0019;

    /**
     * Error for invalid operator address line 1.
     *
     * @var string
     */
    const ERROR_OPERATOR_INVALID_ADDRESS = Code::TC0020;

    /**
     * INACTIVE operator failed twice.
     *
     * @var string
     */
    const INACTIVE_OPERATOR_FAILED_TWICE = Code::TC0021;

    /**
     * Warning for provider not authorized to use requested exam language.
     *
     * @var string
     */
    const WARNING_EXAM_INVALID_LANGUAGE = Code::TC0022;

    /**
     * Warning for exam not started.
     *
     * @var string
     */
    const WARNING_EXAM_NOT_STARTED = Code::TC0023;

    /**
     * Warning for soon-to-expire SAIK.
     *
     * @var string
     */
    const WARNING_SAIK_EXPIRES_SOON = Code::TC0024;

    /**
     * Warning for requesting exam retries.
     *
     * @var string
     */
    const WARNING_EXAM_RETRY = Code::TC0025;

    /**
     * Error for database being unavailable.
     *
     * @var string
     */
    const ERROR_OFFLINE = Code::TC0028;

    /**
     * Warning for users having already passed.
     *
     * @var string
     */
    public const WARNING_EXAM_PASSED = Code::TC0029;

    /**
     * Returns the response status message.
     *
     * @see \TransportCanada\PCOC\Schema\Exam::$AuthorizationStatus
     * @see \TransportCanada\PCOC\Schema\Grade::$ResultStatus
     *
     * @return string
     */
    public function status();

    /**
     * Returns the response results.
     *
     * @see \TransportCanada\PCOC\Response\Exam::$AuthorizeResult
     * @see \TransportCanada\PCOC\Response\Grade::$GetFinalTestResultResult
     *
     * @return mixed
     */
    public function results();

    /**
     * Returns all of the messages associated with the response.
     *
     * @return \TransportCanada\PCOC\Schema\Message[]
     */
    public function messages();

    /**
     * Returns an array of messages keyed by their codes.
     *
     * Example:
     *
     * <samp>
     * array(
     *     'TC0000' => 'Service message for TC0000',
     *     'TC0023' => 'Service message for TC0023',
     * );
     * </samp>
     *
     * @return array
     */
    public function codes();
}
