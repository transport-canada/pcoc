<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Schema;

use TransportCanada\PCOC\Util;

/**
 * Wrapper class for exam/authorization requests.
 */
class Authorize extends Struct\AuthorizationRequestObj
{
    use Util\Lenient;

    /**
     * Define which struct to convert to.
     *
     * @var string
     */
    protected static $decorate = Struct\AuthorizationRequestObj::class;
}
