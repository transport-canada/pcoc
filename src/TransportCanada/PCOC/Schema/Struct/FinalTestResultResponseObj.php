<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Schema\Struct;

/**
 * Test results response object.
 */
class FinalTestResultResponseObj extends BaseResponseObj
{
    /**
     * The number of correctly answered questions (passed) on the online test.
     *
     * Candidates see the total number of questions they answered correctly
     * (out of total answered) when they complete their online test, however
     * they are not shown results for individual questions.
     *
     * @var string
     */
    public $QuestionsPassed = '';

    /**
     * The result of the online test.
     *
     * Candidates see their results when they complete their online test as
     * follows:
     *
     * If they pass: "Congratulations, you passed your boating safety test! Your
     * test mark was n%."
     *
     * If they fail: "You did not pass the boating safety test. The pass mark
     * was 75%, and you obtained n%. You must wait at least 24 hours
     * before taking the test again. Transport Canada recommends that
     * you review the study materials provided by your course provider
     * before taking the test again. An email from your course provider
     * has been sent to you that has the link to retake your test. You
     * must use it within 30 days. To retake the test, click on the link in
     * the email and follow the instructions. Please note, course providers
     * do not charge you additional fees to take the online test for the
     * second time."
     *
     * @see \TransportCanada\PCOC\Enum\ResultStatus
     *
     * @var string
     */
    public $ResultStatus = '';

    /**
     * The percentage of correctly answered questions for the online test.
     *
     * @var string
     */
    public $ScorePercentage = '';

    /**
     * The total number of questions for the online test.
     *
     * @var string
     */
    public $TotalQuestions = '';
}
