<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Util;

/**
 * Enables case-insensitive properties.
 *
 * <samp>
 * <?php
 * class Sensitive {
 *     public $lower = 'lower';
 *     public $UPPER = 'upper';
 * }
 *
 * class NotSensitive extends Sensitive {
 *     use Insensitive;
 * }
 *
 * $s = new Sensitive;
 * $s->lower; // 'lower'
 * $s->upper; // undefined property Sensitive::$upper
 *
 * $i = new NotSensitive;
 * $i->lower; // 'lower'
 * $i->upper; // 'upper' = Insensitive::$UPPER
 * $i->LoWeR; // 'lower' = Insensitive::$lower
 * $i->uPpEr; // 'upper' = Insensitive::$UPPER
 * </samp>
 */
trait Insensitive
{
    /**
     * Cache for insensitive properties.
     *
     * @var array
     */
    protected $insensitive = [];

    /**
     * Attempt to get a property insensitively.
     *
     * @param string $name Property to access
     *
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->{$this->insensitive($name)};
    }

    /**
     * Attempts to set a property insensitively.
     *
     * @param string $name  Property to access
     * @param mixed  $value Property value to set
     */
    public function __set(string $name, $value)
    {
        $this->{$this->insensitive($name)} = $value;
    }

    /**
     * Attempts an `isset` insensitively.
     *
     * @param string $name Property to check
     *
     * @return bool
     */
    public function __isset(string $name)
    {
        return isset($this->{$this->insensitive($name)});
    }

    /**
     * Attempts an `unset` insensitively.
     *
     * @param string $name Property to unset
     */
    public function __unset(string $name)
    {
        unset($this->{$this->insensitive($name)});
        unset($this->insensitive[strtolower($name)]);
    }

    /**
     * Simple method to access object properties insensitively.
     *
     * Also caches the original cased property name for increased performance.
     *
     * @param string $name    Property to de-case
     * @param bool   $refresh Refresh sensitive cache
     *
     * @return string
     */
    public function insensitive(string $name, bool $refresh = false)
    {
        if (empty($this->insensitive) || $refresh) {
            foreach ($this as $property => $value) {
                if (\is_string($property) && 'insensitive' !== $property) {
                    $this->insensitive[strtolower($property)] = $property;
                }
            }
        }

        return $this->insensitive[strtolower($name)] ?? $name;
    }
}
