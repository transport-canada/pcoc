<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Util;

/**
 * Common object hydration functions.
 */
trait Objective
{
    /**
     * Enables mass assignment of properties through the constructor.
     *
     * <samp>
     * <?php
     * class Simple {
     *     public $hello = 'world';
     * }
     *
     * class Constructive {
     *     use Objective;
     * }
     *
     * $simple = new Simple(['hello' => 'everyone']);
     * $construct = new Constructive(['hello' => 'there']);
     * ?>
     *
     * $simple = object(Simple) {
     *     ['hello'] => 'world'
     * }
     *
     * $construct = object(Constructive) {
     *     ['hello'] => 'there'
     * }
     * </samp>
     *
     * @param array|object $with Data for hydration
     */
    public function __construct($with = [])
    {
        $this->fill($with);
    }

    /**
     * Hydrates this object with another array/object.
     *
     * @param array|oject $with   Data for hydration
     * @param bool        $create Create non-existent keys
     */
    public function fill($with, bool $create = false)
    {
        Util::hydrate($this, $with, $create);
    }
}
