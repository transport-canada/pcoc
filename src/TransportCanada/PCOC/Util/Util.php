<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Util;

/**
 * Generic utility functions.
 */
class Util
{
    /**
     * Hydrates an object with another array/object.
     *
     * The $create flag enables adding the key if it's not present.
     *
     * @param object       $object Object to hydrate
     * @param array|object $data   Data for hydration
     * @param bool         $create Create non-existent keys
     */
    public static function hydrate($object, $data, bool $create = false)
    {
        $insensitive = method_exists($object, 'insensitive');

        foreach ($data as $key => $value) {
            if (!\is_string($key)) {
                continue;
            }

            if ($insensitive) {
                // Get the right key from Util\Insensitive
                $key = $object->insensitive($key);
            }

            if (property_exists($object, $key) || $create) {
                $object->$key = $value;
            }
        }
    }

    /**
     * Returns the first array item passing the $callable predicate.
     *
     * @param array    $array    Array to filter
     * @param callable $callback Predicate to run
     *
     * @return mixed
     */
    public static function find(array $array, callable $callback)
    {
        foreach ($array as $key => $value) {
            if ($callback($value, $key)) {
                return $value;
            }
        }
    }
}
