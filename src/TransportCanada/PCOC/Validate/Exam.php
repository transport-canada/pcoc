<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Validate;

use TransportCanada\PCOC\Enum;
use TransportCanada\PCOC\Schema;
use TransportCanada\PCOC\Validate;

/**
 * Validates AuthorizationRequestObj.
 *
 * @see \TransportCanada\PCOC\Schema\Struct\AuthorizationRequestObj
 */
class Exam extends Request
{
    /**
     * Character limit for return URLs.
     *
     * @var int
     */
    public const RETURN_URL_MAX = 500;

    /**
     * Character limit for SAIK.
     *
     * @var int
     */
    public const SAIK_MAX = 200;

    /**
     * Character limit for organization operator ID.
     *
     * @var int
     */
    public const ORGANIZATION_OPERATOR_ID = 10;

    /**
     * Operator validator class.
     *
     * @var string
     */
    public const OPERATOR_INFORMATION_VALIDATOR = Validate\Operator::class;

    /**
     * Array language of enums.
     *
     * @var string
     */
    public const TESTING_LANGUAGE_CD_IN = Enum\Language::class;

    /**
     * Validates the return URL from the official exam.
     *
     * @param string $value URL
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function ReturnURL($value)
    {
        (new Rule\StringRule($value, __FUNCTION__))
            ->max(self::RETURN_URL_MAX);
    }

    /**
     * Validates the request SAIK(TC0000).
     *
     * @param string $value SAIK
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function SAIK($value)
    {
        (new Rule\StringRule($value, __FUNCTION__))
            ->required()
            ->max(self::SAIK_MAX);
    }

    /**
     * Validates the organization ID.
     *
     * @param string $value Org ID
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function OrganizationOperatorId($value)
    {
        (new Rule\StringRule($value, __FUNCTION__))
            ->required()
            ->max(self::ORGANIZATION_OPERATOR_ID);
    }

    /**
     * Validates the operator object.
     *
     * @param \TransportCanada\PCOC\Schema\Struct\OperatorInformationObj $value Operator
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function OperatorInformation($value)
    {
        (new Rule\ObjectRule($value, __FUNCTION__))
            ->required()
            ->instance(Schema\Operator::class)
            ->validate(self::OPERATOR_INFORMATION_VALIDATOR);
    }

    /**
     * Validates the requested language.
     *
     * @param string $value Language enum
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function TestingLanguageCd($value)
    {
        (new Rule\EnumRule($value, __FUNCTION__))
            ->required()
            ->in(\call_user_func([self::TESTING_LANGUAGE_CD_IN, 'enums']));
    }
}
