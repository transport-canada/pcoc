<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Validate\Rule;

use OutOfRangeException;
use InvalidArgumentException;

/**
 * Array validation helper.
 */
class ArrayRule extends Rule
{
    /**
     * Rule constructor.
     *
     * @param mixed  $value Value to validate
     * @param string $name  Name of variable to display
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function __construct($value, $name = '$value')
    {
        parent::__construct($value, $name);

        $this->array();
    }

    /**
     * Checks if the current value is an array.
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return static
     */
    public function array()
    {
        if (!\is_array($this->value)) {
            $this->throw(new InvalidArgumentException($this->name.' must be an array'));
        }

        return $this;
    }

    /**
     * Checks if the current value is in an array.
     *
     * @param mixed $value Value to search in array
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return static
     */
    public function has($value)
    {
        if (!\in_array($value, $this->value)) {
            $this->throw(new OutOfRangeException($this->name.' value isn\'t in array'));
        }

        return $this;
    }
}
