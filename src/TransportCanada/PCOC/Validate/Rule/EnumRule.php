<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Validate\Rule;

use OutOfRangeException;

/**
 * Enum validation helper.
 */
class EnumRule extends Rule
{
    /**
     * Checks if the current value is within an enum array.
     *
     * Slight change to the exception message to be more relevant to enums.
     *
     * @param array $array Haystack enums
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return static
     */
    public function in(array $array)
    {
        if (!\in_array($this->value, $array)) {
            $this->throw(new OutOfRangeException($this->name.' isn\'t a valid enum'));
        }

        return $this;
    }
}
