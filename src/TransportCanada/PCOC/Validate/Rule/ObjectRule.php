<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Validate\Rule;

use InvalidArgumentException;

/**
 * Object validation helper.
 */
class ObjectRule extends Rule
{
    /**
     * Checks if the current object is an instance of...
     *
     * @param object|string $of Class to compare instanceof
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return static
     */
    public function instance($of)
    {
        if (\is_object($of)) {
            $of = \get_class($of);
        }

        if (!($this->value instanceof $of)) {
            $this->throw(new InvalidArgumentException($this->name.' must be an instance of '.$of));
        }

        return $this;
    }

    /**
     * Validates the current object against a validation object.
     *
     * @param object|string $validator Validator object/class to use
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return static
     */
    public function validate($validator)
    {
        if (\is_string($validator)) {
            $validator = new $validator();
        }

        $object = $this->value;
        $properties = get_class_methods($validator);

        foreach ($properties as $property) {
            $validator->$property($object->$property);
        }

        return $this;
    }
}
