<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Validate\Rule;

use TransportCanada\PCOC\Exception\ValidationException;
use UnexpectedValueException;
use Throwable;

/**
 * Base validation helper.
 */
class Rule
{
    /**
     * Name of property or variable to display.
     *
     * @var string
     */
    protected $name;

    /**
     * Value to validate.
     *
     * @var mixed
     */
    protected $value;

    /**
     * Rule constructor.
     *
     * @param mixed  $value Value to validate
     * @param string $name  Name of variable to display
     */
    public function __construct($value, $name = '$value')
    {
        $this->value = $value;
        $this->name = $name;
    }

    /**
     * Checks if our value is not null.
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return static
     */
    public function required()
    {
        if (empty($this->value)) {
            $this->throw(new UnexpectedValueException($this->name.' is required'));
        }

        return $this;
    }

    /**
     * Exception wrapper and thrower.
     *
     * @param \Throwable $ex PHP exceptions
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    protected function throw(Throwable $ex)
    {
        throw new ValidationException($ex->getMessage(), $ex->getCode(), $ex);
    }
}
