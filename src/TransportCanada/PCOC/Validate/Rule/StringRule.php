<?php

/*
 * Copyright 2018 Jonathan Ginn <ginn@gammacontrol.ca>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace TransportCanada\PCOC\Validate\Rule;

use InvalidArgumentException;
use UnexpectedValueException;

/**
 * String validation helper.
 */
class StringRule extends Rule
{
    /**
     * String rule constructor.
     *
     * @param mixed  $value Value to validate
     * @param string $name  Name of variable to display
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     */
    public function __construct($value, $name = '$value')
    {
        parent::__construct($value, $name);

        $this->string();
    }

    /**
     * Checks if the current value is a string.
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return static
     */
    public function string()
    {
        if (!\is_string($this->value)) {
            $this->throw(new InvalidArgumentException($this->name.' must be a string'));
        }

        return $this;
    }

    /**
     * Checks if the current string is under max characters.
     *
     * @param int $length Maximum string length
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return static
     */
    public function max(int $length)
    {
        if (\strlen($this->value) > $length) {
            $this->throw(new UnexpectedValueException($this->name.' must not exceed '.$length.' characters'));
        }

        return $this;
    }

    /**
     * Checks if the current string has a minimum amount of characters.
     *
     * @param int $length Minimum string length
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return static
     */
    public function min(int $length)
    {
        if (\strlen($this->value) < $length) {
            $this->throw(new UnexpectedValueException($this->name.' must contain at least '.$length.' characters'));
        }

        return $this;
    }

    /**
     * Checks if the current string return true on preg_match.
     *
     * @param string $regex Regex pattern to match
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return static
     */
    public function regex(string $regex)
    {
        if (!preg_match($regex, $this->value)) {
            $this->throw(new UnexpectedValueException($this->name.' doesn\'t match regex '.$regex));
        }

        return $this;
    }

    /**
     * Checks if the current string has a specific amount of characters.
     *
     * @param int $length String length
     *
     * @throws \TransportCanada\PCOC\Exception\ValidationException
     *
     * @return static
     */
    public function length(int $length)
    {
        if (\strlen($this->value) !== $length) {
            $this->throw(new UnexpectedValueException($this->name.' must contain exactly '.$length.' characters'));
        }

        return $this;
    }
}
